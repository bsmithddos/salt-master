#!/usr/bin/php
<?php
/* date: 24-May-2016
 * auth: bob smith
 * info: perform the same effective task of mysql_secure_installation in an automated fashion
 */
$dbpass = "";

try
  {
  $db = new PDO("mysql:host=localhost;dbname=mysql","root");
  }
catch (PDOException $e)
  {
  if (strstr($e->getMessage(), "Access denied for user") === false)
    {
    printf("%s\n", $e->getMessage());
    exit(1);
    }
  else
    {
    printf("mysql installation already secured\n");
    exit(0);
    }
  }
$query[1] = "UPDATE mysql.user SET Password=PASSWORD('".$dbpass."') WHERE User='root';";
$query[2] = "DELETE FROM mysql.user WHERE User=''";
$query[3] = "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')";
$query[4] = "DROP DATABASE test";
$query[5] = "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'";
$query[6] = "FLUSH PRIVILEGES";

foreach($query as $k => &$v)
  {
  $run = $db->prepare($v);
  try
    {
    $run->execute();
    }
  catch (PDOException $e)
    {
    printf("%s\n", $e->getMessage());
    exit(1);
    }
  }
?>
