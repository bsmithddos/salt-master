#!/bin/bash
# date: 25-Nov-2015
# auth: bob smith
# info: csf presence test

if [ $(/usr/sbin/csf -v | egrep -c 'csf: v8.08 \(generic\)') -gt 0 ]
  then
    exit 0
  else
    exit 1
  fi
