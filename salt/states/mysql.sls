include:
  - states/php

mysql_pkgs:
  pkg.installed:
    - pkgs:
      - mysql
      - mysql-server

mysql_svc:
  service.running:
    - name: mysqld
    - enable: true
    - reload: true
    - require:
      - pkg: mysql_pkgs

mysql_secure_script_copy:
  file.managed:
    - name: /opt/mysql_secure_install.php
    - source: salt://files/mysql_secure_install.php
    - user: root
    - group: root
    - mode: 744
    - require:
      - service: mysql_svc

mysql_secure_root_pass_set:
  file.line:
    - name: /opt/mysql_secure_install.php
    - content: "$dbpass = \"{{ pillar['mysql']['default_pass'] }}\";"
    - match: "dbpass = *"
    - mode: replace
    - watch:
      - file: mysql_secure_script_copy

mysql_run_secure:
  cmd.run:
    - name: /opt/mysql_secure_install.php
    - cwd: /opt
    - require:
      - file: mysql_secure_root_pass_set
