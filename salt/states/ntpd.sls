ntp_install:
  pkg.installed:
    - pkgs:
      - ntp
      - ntpdate

ntp_restart:
  service.running:
    - name: ntpd
    - enable: true
    - restart: true
    - require:
      - pkg: ntp_install
