update_amazon_main_repo:
  file.managed:
    - name: /etc/yum.repos.d/amzn-main.repo
    - source: salt://files/update_amzn_main.repo
    - source_hash: md5=998ba06783c895e997ba5c7e57941703

update_clear_cache:
  cmd.run:
    - name: "yum clean all"
    - require:
      - file: update_amazon_main_repo

update:
  cmd.run:
    - name: "yum update -y"
    - require:
      - cmd: update_clear_cache
