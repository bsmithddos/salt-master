skel_dir:
  file.recurse:
    - name: /etc/skel/
    - source: salt://files/skel_dir/
    - user: root
    - group: root
    - file_mode: 644
    - include_empty: true
    - dir_mode: 755

skel_dir_update_root:
  file.recurse:
    - name: /root/
    - source: salt://files/skel_dir/
    - user: root
    - group: root
    - file_mode: 644
    - include_empty: true
    - dir_mode: 755
