common_pkgs:
  pkg.installed:
    - pkgs:
      - nano
      - git
      - wget
      - net-tools
      - mlocate
