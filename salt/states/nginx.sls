nginx_install:
  pkg.installed:
    - name: nginx-1.10.0-3.el6.x86_64.rpm
    - sources:
      - nginx: salt://files/nginx-1.10.0-3.el6.x86_64.rpm
    - reinstall: true
    - allow_updates: false
