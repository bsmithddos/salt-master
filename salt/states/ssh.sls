ssh_banner:
  file.managed:
    - name: /etc/ssh/banner
    - source: salt://files/ssh_banner
    - source_hash: md5=7a42d5f2f65cf16e8a5c3a6a0715137a
    - user: root
    - group: root
    - mode: 644

sshd_config:
  file.managed:
    - name: /etc/ssh/sshd_config
    - source: salt://files/ssh_sshd_config
    - source_hash: md5=f943a43bf62d6f2784e46394388eb040
    - user: root
    - group: root
    - mode: 644

sshd_restart:
  service.running:
    - name: sshd
    - enable: true
    - restart: true
    - watch:
      - file: sshd_config
      - file: ssh_banner
