php_pkgs:
  pkg.installed:
    - pkgs:
      - php-cli 
      - php-common
      - php-fpm
      - php-mbstring
      - php-mysqlnd
      - php-pdo

php_fpm_conf:
  file.managed:
    - name: /etc/php-fpm.d/www.conf
    - source: salt://files/php_www.conf
    - source_hash: md5=32b90d0b70c9ec9e55ce50e8b5731410
    - require:
      - pkg: php_pkgs

php_fpm_start:
  service.running:
    - name: php-fpm
    - enable: true
    - reload: true
    - watch:
      - file: php_fpm_conf
