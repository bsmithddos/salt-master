#include:
#  - states/nginx
#  - states/php
#  - states/mysql

joomla_user:
  user.present:
    - name: joomla

joomla_install:
  archive.extracted:
    - name: /home/joomla/www/
    - source: salt://files/Joomla_3.5.1-Stable-Full_Package.zip
    - source_hash: md5=5a441bf534d2c4a631e590ef1b2a1491
    - archive_format: zip
    - user: joomla
    - group: joomla
    - require:
      - user: joomla_user

joomla_nginx_permissions:
  file.directory:
    - name: /home/joomla
    - dir_mode: 755

joomla_nginx_conf:
  file.managed:
    - name: /etc/nginx/sites-available/joomla.conf
    - source: salt://files/joomla_nginx.conf
    - user: nginx
    - group: nginx
    - mode: 0644

joomla_nginx_reload:
  service.running:
    - name: nginx
    - enable: true
    - reload: true
    - watch:
      - file: joomla_nginx_conf


