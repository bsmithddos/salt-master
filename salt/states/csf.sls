csf_check_script_copy:
  file.managed:
    - name: /usr/local/bin/csf_installed.sh
    - source: salt://files/csf_installed.sh
    - source_hash: md5=5e1bfeffb36737c34c5f4e58508052f2
    - user: root
    - group: root
    - mode: 755

csf_needed_pkgs:
  pkg.installed:
    - pkgs:
      - perl
      - perl-libwww-perl
    - unless: /usr/local/bin/csf_installed.sh
    - require:
      - file: csf_check_script_copy

csf_copy_tar:
  file.managed:
    - name: /opt/csf.tgz
    - source: salt://files/csf_csf.tgz
    - source_hash: md5=e632cfa10ffe4e31ada670a089aa5b20
    - user: root
    - group: root
    - mode: 644
    - unless: /usr/local/bin/csf_installed.sh
    - require:
      - file: csf_check_script_copy

csf_untar:
  module.run:
    - name: archive.tar
    - tarfile: /opt/csf.tgz
    - cwd: /opt/
    - options: xf
    - sources: []
    - unless: /usr/local/bin/csf_installed.sh
    - require:
      - file: csf_copy_tar
      - file: csf_check_script_copy

csf_install:
  cmd.run:
   - name: /opt/csf/install.sh
   - cwd: /opt/csf/
   - unless: /usr/local/bin/csf_installed.sh
   - require:
     - file: csf_check_script_copy
     - pkg: csf_needed_pkgs
     - module: csf_untar

csf_conf_setup:
  file.managed:
    - name: /etc/csf/csf.conf
    - source: salt://files/csf_csf.conf
    - source_hash: md5=560dc3b986f684d2997ed37d639aca90
    - user: root
    - group: root
    - mode: 644
    - require:
      - cmd: csf_install

csf_service:
  service.running:
    - name: csf
    - enable: true
    - reload: true
    - cwd: /etc/csf/
    - watch:
      - cmd: csf_install
      - file: csf_conf_setup

# reserved for future use
#{# for comment, addr in salt['pillar.get']('csf:whitelist').items() #}
#{% set comment = 0 %}
#{% set addr = 0 %}
#csf_whitelist_{{ comment }}:
#  cmd.run:
#    - name: "csf -a {{ addr }} {{ comment }}"
#    - require:
#      - service: csf_service
#{# endfor #}

