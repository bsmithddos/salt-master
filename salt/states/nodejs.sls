nodejs_user:
  user.present:
    - name: nodejs
    - createhome: false
    - shell: /sbin/nologin

nodejs_install:
  archive.extracted:
    - name: /usr/local/nodejs
    - source: salt://files/node-v4.4.5-linux-x64.tar.gz
    - source_hash: md5=feccbb026b0fc2b8feeb9d20a6eb3e54
    - archive_format: tar
    - user: nodejs
    - group: nodejs
    - require:
      - user: nodejs_user

nodejs_symlink_node:
  file.symlink:
    - name: /usr/local/bin/node
    - target: /usr/local/nodejs/bin/node
    - user: nodejs
    - group: nodejs
    - mode: 0755
    - require:
      - archive: nodejs_install
      - user: nodejs_user

nodejs_symlink_npm:
  file.symlink:
    - name: /usr/local/bin/npm
    - target: /usr/local/nodejs/bin/node
    - user: nodejs
    - group: nodejs
    - mode: 0755
    - require:
      - archive: nodejs_install
      - user: nodejs_user
