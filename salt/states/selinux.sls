selinux:
  file.managed:
    - name: /etc/selinux/config
    - source: salt://files/selinux_config
    - source_hash: md5=9743cfd3d26e1f5ff815213185103e0e
    - user: root
    - group: root
    - file_mode: 644
    - dir_mode: 644
