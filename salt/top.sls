base:
  '*':
    - states/update
    - states/common
    - states/skel_dir
    - states/ntpd
    - states/ssh
    - states/selinux
    - states/csf

  'test-webserver*':
    - states/nginx
    - states/php
    - states/nodejs
    - states/mysql
