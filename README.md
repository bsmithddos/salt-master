# Salt Master
## Formulas & Files

top.sls dump
```
base:
  '*':
    - states/update
    - states/common
    - states/skel_dir
    - states/ntpd
    - states/ssh
    - states/selinux
    - states/csf

  'webserver*':
    - states/nginx
    - states/php
    - states/nodejs
    - states/mysql
```
